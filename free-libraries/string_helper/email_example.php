<?
// INCLUDE FILES
include('includes/stringhelper.php');

// VARIABLES
$results	= '';

// PROCESS CONVERSION
if(isset($_POST['email']) && strlen(trim($_POST['email'])) > 0)
{
	$email		= new StringHelper();
	$isValid	= $email->validateEmail($_POST['email']);
	if($isValid)
		$results	= "<div>Email is valid</div>";
	else
		$results	= "<div>Email is NOT valid</div>";
		
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Validate Email</title>
		<style>
		form.fatform input[type=text]{
			display:block;
			height: 30px;
			width: 300px;
			padding: 5px 10px 5px 10px;
			color: #666;
			font-size: 16px;
			font-family: sans-serif;
			margin-bottom: 10px;
		}
		form.fatform input[type=submit]{
			margin: 6px 0 0 0;
			width: 75px;
		}
		#results{
			margin: 10px 0 10px 10px;
		}
		#results div{
			margin: 4px;
			font-family: serif;
			font-size: 18px;
			color: #888;
		}
		span.bold{
			font-family: serif;
			font-size: 18px;
			color: #000;
			font-weight:bold
		}
		</style>
	</head>
	
	<body>
		<form name="email" id="validate_email" action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="fatform">
			<input type="text" name="email">
			<input type="submit" value="GO">
		</form>
		<div id="results"><?=$results?></div>
	</body>
</html>