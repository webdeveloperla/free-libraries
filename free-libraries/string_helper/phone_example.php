<?
// INCLUDE FILES
include('includes/stringhelper.php');

// VARIABLES
$results	= '';
$separator	= '-';

// PROCESS CONVERSION
if(isset($_POST['phone_number']) && strlen(trim($_POST['phone_number'])) > 0)
{
	$area_code	= ($_POST['area_code'] == '1') ? true : false;
	$phone		= new StringHelper();
	$normalized	= $phone->normalizePhone($_POST['phone_number'],$area_code,$separator);
	$results = "<div>".$normalized."</div>";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Normalize Phone</title>
		<style>
		form.fatform input[type=text]{
			display:block;
			height: 30px;
			width: 300px;
			padding: 5px 10px 5px 10px;
			color: #666;
			font-size: 16px;
			font-family: sans-serif;
			margin-bottom: 10px;
		}
		form.fatform input[type=submit]{
			margin: 6px 0 0 0;
			width: 75px;
		}
		#results{
			margin: 10px 0 10px 10px;
		}
		#results div{
			margin: 4px;
			font-family: serif;
			font-size: 18px;
			color: #888;
		}
		span.bold{
			font-family: serif;
			font-size: 18px;
			color: #000;
			font-weight:bold
		}
		</style>
	</head>
	
	<body>
		<form name="phone" id="normalize_phone" action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="fatform">
			<input type="text" name="phone_number">
			<input type=checkbox name="area_code" value='1' checked>Contains Area Code<br />
			<input type="submit" value="GO">
		</form>
		<div id="results"><?=$results?></div>
	</body>
</html>