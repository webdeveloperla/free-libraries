<?
// INCLUDE FILES
include('includes/stringhelper.php');

// VARIABLES
$results = '';

// PROCESS CONVERSION
if(isset($_POST['original_string']) && strlen(trim($_POST['original_string'])) > 0)
{
	$convert	= new StringHelper();
	$converted	= $convert->convertToPhoneticAlphabet($_POST['original_string'],$_POST['conversion_type']);
	foreach($converted as $c)
	{
		preg_match('/\W/', $c, $char);
		if(count($char) > 0)
		{
			// If character is not a-z, just show the character
			$results .= "<div>".$char[0]."</div>";
		}else{
			$results .= "<div><span class='bold'>".substr($c,0,1)."</span> as in <span class='bold'>".$c."</span></div>";
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Phonetic Alphabet</title>
		<style>
		form.fatform input[type=text]{
			display:block;
			height: 30px;
			width: 300px;
			padding: 5px 10px 5px 10px;
			color: #666;
			font-size: 16px;
			font-family: sans-serif;
			margin-bottom: 10px;
		}
		form.fatform input[type=submit]{
			margin: 6px 0 0 0;
			width: 75px;
		}
		#results{
			margin: 10px 0 10px 10px;
		}
		#results div{
			margin: 4px;
			font-family: serif;
			font-size: 18px;
			color: #888;
		}
		span.bold{
			font-family: serif;
			font-size: 18px;
			color: #000;
			font-weight:bold
		}
		</style>
	</head>
	
	<body>
		<form name="conversion" id="conversion" action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="fatform">
			<input type="text" name="original_string">
			<input type=radio name="conversion_type" value='military'>Military &nbsp;&nbsp;
			<input type=radio name="conversion_type" value='civilian' checked>Civilian<br />
			<input type="submit" value="GO">
		</form>
		<div id="results"><?=$results?></div>
	</body>
</html>
